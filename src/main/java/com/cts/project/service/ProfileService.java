package com.cts.project.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.cts.project.model.Profile;
import com.sun.tools.javac.util.List;

@Service
public class ProfileService {
	private ArrayList<Profile> users;

	public ProfileService() {
		users = new ArrayList<Profile>();
		users.add(new Profile("Deepu", "Presenter", "pass1"));
		users.add(new Profile("Achyuth", "Panelist", "pass2"));
		users.add(new Profile("Super User", "Host", "admin"));
	}

	public Profile findUser(String name) {
		for (Profile p : users) {
			if (p.getUserName().equalsIgnoreCase(name)) {
				return p;
			}
		}
		return null;
	}

}