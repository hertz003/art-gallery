package com.cts.project.model;

public class Profile {
	private String UserName;
	private String role;
	private String password;

	public Profile(String UserName, String role, String password) {
		super();
		// TODO Auto-generated constructor stub
		this.UserName = UserName;
		this.role = role;
		this.password = password;

	}

	public Profile() {
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}